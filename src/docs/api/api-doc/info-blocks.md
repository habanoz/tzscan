### Sample result

```json
[
  {
    "hash":"BMVQ3ujNct6YDhwyuzRGPXCk4ZBpwyB3WtMQFGFrCw5a9KQjAEk",
    "predecessor_hash":"BMFFDcfeEBNEf1XCgyNUi5XaaQefs2iGMj5DV8rkdsPozXRL5Tj",
    "fitness":"00 000000000007e597",
    [...]
  },
  {
    "hash":"BMFFDcfeEBNEf1XCgyNUi5XaaQefs2iGMj5DV8rkdsPozXRL5Tj",
    "predecessor_hash":"BLjfedJ2WfdWW2UrFTPod8Uq7dVdDRZBf7ULqkdj4zeg4tJqHr5",
    "fitness":"00 000000000007e589",
    [...]
  },
  {
    "hash":"BLjfedJ2WfdWW2UrFTPod8Uq7dVdDRZBf7ULqkdj4zeg4tJqHr5",
    "predecessor_hash":"BLedLYKsQ3zExDdVyLw219wy9W8u8CzYcJzpK4ppZCnCVQ2F4kN",
    "fitness":"00 000000000007e581",
    [...]
  },
  [...]
]
```
