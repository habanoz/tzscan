
### Sample Result

```json
{
   "hash":"BMVCUZCPwFMRD2SJkVMyVsVKW7bmx5PoVoEoN9TS1m9Hu2gdGpW",
   "predecessor_hash":"BLq6BfiKfjHFCcJwseK4cVe3DVXQtjDRBJu9cF6BMoUMbXPdPj2",
   "fitness":"00 00000000000785a5",
   "timestamp":"2018-02-02T10:52:50Z",
   "validation_pass":1,
   ""operations":[[{"hash":"opYAPTdbCtwSUtSMnHNPF6ssVRQeV2StADMH2mZjQfKij2JLreC","branch":"","data":""}]],
   "protocol":{ "name":"ProtoAlpha[...]", "hash":"ProtoALpha[...]"},
   "test_protocol":{"name":"ProtoALpha[...]", "hash":"ProtoALpha[...]"},
   "network":"NetXj4yEEKnjaK8",
   "test_network":"PLACEHOLDER_test_network",
   "test_network_expiration":"PLACEHOLDER_TEST_NETWORK_EXPIRATION",
   "baker":"tz1ey28xfyVvtPRPN9d43Wbf1vkPs868CGXM",
   "nb_operations":5,
   "priority":0,
   "level":63522,
   "commited_nonce_hash":"nceUShE51M5EnRoNVTVkGSKr8Q8Eqk2GJG9bnaAEejmz4TXei2q4S",
   "pow_nonce":"0aff540bd675d6cb",
   "proto":1,
   "data":"--",
   "signature":"--"
}
```
